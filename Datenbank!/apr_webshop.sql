-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 24. Mrz 2021 um 15:20
-- Server-Version: 10.4.14-MariaDB
-- PHP-Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `apr_webshop`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `artikel`
--

CREATE TABLE `artikel` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `price` decimal(11,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `artikel`
--

INSERT INTO `artikel` (`id`, `name`, `description`, `price`) VALUES
(7, 'Nintendo Switch', 'Nintendo Switch, Blau und Rot mit 2 Spielen', '400'),
(8, 'PS5', 'Playstation 5, weiß', '500'),
(9, 'PC', 'Acer Predator', '1200'),
(10, 'Laptop', 'HP', '750'),
(11, 'Xbox', 'Series X', '450');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `benutzer`
--

CREATE TABLE `benutzer` (
  `id` int(11) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `passwort` varchar(45) NOT NULL,
  `benutzername` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `benutzer`
--

INSERT INTO `benutzer` (`id`, `first_name`, `last_name`, `passwort`, `benutzername`) VALUES
(3, 'Alex', 'Venier', '123', 'Alex'),
(4, 'Lukas', 'Ladner', '22', 'Lukas'),
(5, 't', 't', 't', 't'),
(9, 'Alex', 'Venier', '123', 'Alex');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bestellung`
--

CREATE TABLE `bestellung` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `Benutzer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `bestellung`
--

INSERT INTO `bestellung` (`id`, `date`, `Benutzer_id`) VALUES
(9, '2021-03-24', 5),
(10, '2021-03-24', 5);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bestellung_details`
--

CREATE TABLE `bestellung_details` (
  `id` int(11) NOT NULL,
  `Artikel_id` int(11) NOT NULL,
  `Bestellung_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `bestellung_details`
--

INSERT INTO `bestellung_details` (`id`, `Artikel_id`, `Bestellung_id`, `amount`) VALUES
(10, 7, 9, 2),
(11, 8, 9, 1),
(12, 9, 10, 1);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `artikel`
--
ALTER TABLE `artikel`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `benutzer`
--
ALTER TABLE `benutzer`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `bestellung`
--
ALTER TABLE `bestellung`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Bestellung_Benutzer1_idx` (`Benutzer_id`);

--
-- Indizes für die Tabelle `bestellung_details`
--
ALTER TABLE `bestellung_details`
  ADD PRIMARY KEY (`id`,`Artikel_id`,`Bestellung_id`),
  ADD KEY `fk_Artikel_has_Bestellung_Bestellung1_idx` (`Bestellung_id`),
  ADD KEY `fk_Artikel_has_Bestellung_Artikel_idx` (`Artikel_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `artikel`
--
ALTER TABLE `artikel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT für Tabelle `benutzer`
--
ALTER TABLE `benutzer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT für Tabelle `bestellung`
--
ALTER TABLE `bestellung`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT für Tabelle `bestellung_details`
--
ALTER TABLE `bestellung_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `bestellung`
--
ALTER TABLE `bestellung`
  ADD CONSTRAINT `fk_Bestellung_Benutzer1` FOREIGN KEY (`Benutzer_id`) REFERENCES `benutzer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `bestellung_details`
--
ALTER TABLE `bestellung_details`
  ADD CONSTRAINT `fk_Artikel_has_Bestellung_Artikel` FOREIGN KEY (`Artikel_id`) REFERENCES `artikel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Artikel_has_Bestellung_Bestellung1` FOREIGN KEY (`Bestellung_id`) REFERENCES `bestellung` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
