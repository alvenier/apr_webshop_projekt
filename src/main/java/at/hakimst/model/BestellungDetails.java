package at.hakimst.model;

public class BestellungDetails {
    private int id;
    private Artikel artikel;
    private Bestellung bestellung;
    private String artikelName;
    private double price;
    private int amount;

    public BestellungDetails(int id, Artikel artikel, Bestellung bestellung, int amount) {
        super();
        this.id= id;
        this.artikel = artikel;
        this.bestellung = bestellung;
        this.amount = amount;
        artikelName = artikel.getName();
        price = artikel.getPrice() * amount;
    }

    public BestellungDetails(Artikel artikel, Bestellung bestellung, int amount) {
        this.artikel = artikel;
        this.bestellung = bestellung;
        this.amount = amount;
        artikelName = artikel.getName();
        price = artikel.getPrice() * amount;
    }

    public BestellungDetails(Artikel artikel,  int amount){
        this.artikel = artikel;
        this.amount = amount;
        artikelName = artikel.getName();
        price = artikel.getPrice() * amount;
    }

    public int getId() {
        return id;
    }

    public Artikel getArtikel() {
        return artikel;
    }

    public Bestellung getBestellung() {
        return bestellung;
    }

    public void setBestellung(Bestellung bestellung) {
        this.bestellung = bestellung;
    }

    public int getAmount() {
        return amount;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public String getArtikelName() {
        return artikelName;
    }
}
