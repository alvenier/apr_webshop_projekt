package at.hakimst.model;

import java.util.Objects;

public class Artikel {
    public int ID;
    public String name;
    public String describtion;
    private double price;

    public Artikel(int id, String name, String describtion, double price){
        super();
        this.ID = id;
        this.name = name;
        this.describtion = describtion;
        this.price = price;

    }

    public Artikel(String name, String describtion, double price){
        this.name = name;
        this.describtion = describtion;
        this.price = price;
    }
    //Getter
    public int getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public String getDescribtion() {
        return describtion;
    }

    public double getPrice() {
        return price;
    }
    //Setter
    public void setName(String name) {
        this.name = name;
    }

    public void setDescribtion(String describtion) {
        this.describtion = describtion;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setID(Integer id) {
        this.ID = id;
    }

    @Override
    public String toString() {
        return "Artikel{" +
                "id=" + ID +
                ", name='" + name + '\'' +
                ", description='" + describtion + '\'' +
                ", price=" + price +
                '}';
    }

    public String anzeigen(){
        return "Name: " + name + ", Preis: " + price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Artikel artikel = (Artikel) o;
        return ID == artikel.ID &&
                Double.compare(artikel.price, price) == 0 &&
                Objects.equals(name, artikel.name) &&
                Objects.equals(describtion, artikel.describtion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ID, name, describtion, price);
    }
}
