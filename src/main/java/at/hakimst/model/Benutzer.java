package at.hakimst.model;

import java.util.Date;
import java.util.Objects;

public class Benutzer {
    private int id;
    private String first_name;
    private String last_name;

    private String passwort;
    private String benutzername;


    public Benutzer(int id, String first_name, String last_name, String passwort, String benutzername){
        super();
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;

        this.passwort = passwort;
        this.benutzername = benutzername;
    }
    public Benutzer( String first_name, String last_name, String passwort, String benutzername){
        super();
        this.first_name = first_name;
        this.last_name = last_name;

        this.passwort = passwort;
        this.benutzername = benutzername;
    }

    public int getId() {
        return id;
    }

    public String getPasswort() {
        return passwort;
    }

    public String getBenutzername() {
        return benutzername;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }



    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Benutzer{" +
                "id=" + id +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", passwort='" + passwort + '\'' +
                ", benutzername='" + benutzername + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Benutzer benutzer = (Benutzer) o;
        return id == benutzer.id && Objects.equals(first_name, benutzer.first_name) && Objects.equals(last_name, benutzer.last_name) && Objects.equals(passwort, benutzer.passwort) && Objects.equals(benutzername, benutzer.benutzername);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, first_name, last_name, passwort, benutzername);
    }
}
