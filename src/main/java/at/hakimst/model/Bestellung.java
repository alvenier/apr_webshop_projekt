package at.hakimst.model;

import java.util.Date;

public class Bestellung {
    private int id;
    private Date date;
    private Benutzer benutzer;

    public Bestellung(int id, Date date, Benutzer benutzer) {
        this.id = id;
        this.date = date;
        this.benutzer = benutzer;
    }

    public Bestellung(Date date, Benutzer benutzer) {
        this.date = date;
        this.benutzer = benutzer;
    }




    public int getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    /**
     *
     * @return
     */
    public Benutzer getBenutzer() {
        return benutzer;
    }

    public void setId(int id) {
        this.id = id;
    }
}
