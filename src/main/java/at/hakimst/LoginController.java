package at.hakimst;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import at.hakimst.db.BenutzerDaoImpl;
import at.hakimst.model.Benutzer;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

public class LoginController {

    public static final Benutzer admin = new Benutzer(-1,"admin", "admin", "admin", "admin");;

    @FXML
    private TextField benutzername_feld, passwort_feld;

    public LoginController() {

    }
    @FXML
    private void buttonlogin() throws IOException {
        anmelden();
    }
    @FXML
    private void buttonsign() throws IOException {
        App.setRoot("sign");

    }

    /**
     * Eine Methode umd zu überprüfen ob der Benutzer vorhanden ist
     * @param b
     * @param benutzername
     * @param pw
     * @return
     */
    private boolean userexist(Benutzer b, String benutzername, String pw){
        return b.getBenutzername().equals(benutzername) && b.getPasswort().equals(pw);
    }

    /**
     * Hier überprüfen erledigen wir den Login und geben den eingeloggten Benutzer, sofern er vorhanden ist an den Main Controller weiter
     * @throws IOException
     */
    private void anmelden() throws IOException{
        String username = this.benutzername_feld.getText().trim();
        String passwort = this.passwort_feld.getText().trim();
        if(isAdmin(username, passwort)){
            App.setRoot("adminmenu");
            return;
        }
        if(username.isEmpty() || passwort.isEmpty()){
            System.out.println("Bitte füllen sie alle Felder aus!");

        } else {
            boolean angemeldet = false;
            BenutzerDaoImpl dao = new BenutzerDaoImpl();
            List<Benutzer> benutzerlist = new ArrayList<>();
            benutzerlist = dao.getAllObjects();
            Benutzer angmeldetbenutzer = null;
            for (Benutzer b : benutzerlist){
                if(userexist(b, username, passwort)){
                    angemeldet = true;
                    angmeldetbenutzer = b;
                    break;
                }
            }
            if(angemeldet){
                    App.setRoot("main");
                    MainController.getInstance().setBenutzer(angmeldetbenutzer);

            }else {
                System.out.println("Benutzername oder Passwort sind falsch!");
            }
        }


    }

    /**
     * Hier überprüfen wir ob, die eingebenen Daten vom Admin stammen
     * @param username
     * @param passwort
     * @return
     */
    private boolean isAdmin(String username, String passwort){
        return username.equals(admin.getBenutzername()) && passwort.equals(admin.getPasswort());
    }



}
