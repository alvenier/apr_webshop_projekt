package at.hakimst.db;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBManager {
    private static Connection connection;
    private final static String HOST = "127.0.0.1";
    private final static String PORT = "3306";
    private final static String DB_NAME = "apr_webshop";
    private final static String USER = "root";
    private final static String PWD = "";


    private DBManager() {
    }




    public static Connection getConnection() {
        if (connection != null) {
            return connection;
        }
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://" + HOST + ":"+ PORT + "/" + DB_NAME, USER, PWD);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }
}
