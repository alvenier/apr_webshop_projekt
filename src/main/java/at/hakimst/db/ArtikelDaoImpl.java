package at.hakimst.db;

import at.hakimst.model.Artikel;
import at.hakimst.model.Benutzer;

import java.sql.PreparedStatement;
import java.sql.*;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ArtikelDaoImpl implements IDao<Artikel> {
    private final static String table_name = "artikel";
    @Override
    public List<Artikel> getAllObjects() {
        List<Artikel> artikelList = new ArrayList<>();
        String sql = "SELECT * FROM " + table_name;
        try{
            PreparedStatement pst = DBManager.getConnection().prepareStatement(sql);
            ResultSet set = pst.executeQuery();
            int numCols = set.getMetaData().getColumnCount();
            while (set.next()) {
                Artikel invoice = new Artikel(set.getInt(1), set.getString(2), set.getString(3), set.getDouble(4));
                artikelList.add(invoice);
            }
            return artikelList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return artikelList;
    }

    @Override
    public Artikel getObject(int id) {
        String sql = "SELECT * FROM " + table_name + " WHERE id = ?";
        try {
            PreparedStatement ps = DBManager.getConnection().prepareStatement(sql);
            ps.setInt(1,id);
            ResultSet set = ps.executeQuery();
            int numCols = set.getMetaData().getColumnCount();
            set.next();
            Artikel student =  new Artikel((Integer) id, set.getString(2), set.getString(3), set.getDouble(4));
            return student;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void addObject(Artikel artikel) {
        try {
        String insertSql = "INSERT INTO " + table_name + "(name, description, price) VALUES (?,?,?)";
        PreparedStatement ps = DBManager.getConnection().prepareStatement(insertSql, Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, artikel.getName());
        ps.setString(2, artikel.getDescribtion());
        ps.setDouble(3, artikel.getPrice());
        ps.executeUpdate();
        ResultSet keys = ps.getGeneratedKeys();
        keys.next();
    } catch (Exception e) {
        e.printStackTrace();
    }
    }

    @Override
    public void updateObject(Artikel artikel) {
        try {
            String insertSql = "UPDATE " + table_name + " SET name = ?, description = ?, price = ? WHERE id = ?";
            PreparedStatement ps = DBManager.getConnection().prepareStatement(insertSql);
            ps.setString(1, artikel.getName());
            ps.setString(2, artikel.getDescribtion());
            ps.setDouble(3, artikel.getPrice());
            ps.executeUpdate();
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteObject(int id) {
        String sql = "DELETE FROM " + table_name + " WHERE id = ?";
        try {
            PreparedStatement ps = DBManager.getConnection().prepareStatement(sql);
            ps.setInt(1,id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    }

