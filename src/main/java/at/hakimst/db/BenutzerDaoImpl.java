package at.hakimst.db;

import at.hakimst.model.Benutzer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class BenutzerDaoImpl implements IDao<Benutzer> {
    private final static String table_name = "benutzer";

    @Override
    public List<Benutzer> getAllObjects() {
        List<Benutzer> benutzerList = new ArrayList<>();
        String sql = "SELECT * FROM " + table_name;
        try{
            PreparedStatement pst = DBManager.getConnection().prepareStatement(sql);
            ResultSet set = pst.executeQuery();
            int numCols = set.getMetaData().getColumnCount();
            while (set.next()) {
                Benutzer invoice = new Benutzer(set.getInt(1), set.getString(2), set.getString(3), set.getString(4), set.getString(5) );
                benutzerList.add(invoice);
            }
            return benutzerList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return benutzerList;

    }

    @Override
    public Benutzer getObject(int id) {
        String sql = "SELECT * FROM " + table_name + " WHERE id = ?";
        try {
            PreparedStatement ps = DBManager.getConnection().prepareStatement(sql);
            ps.setInt(1,id);
            ResultSet set = ps.executeQuery();
            int numCols = set.getMetaData().getColumnCount();
            set.next();
            Benutzer student =  new Benutzer((int) id, set.getString(2), set.getString(3), set.getString(4), set.getString(5) );
            return student;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;

    }

    @Override
    public void addObject(Benutzer student) {
        try {
            String insertSql = "INSERT INTO " + table_name + "(first_name, last_name, passwort, benutzername) VALUES (?,?,?,?)";
            PreparedStatement ps = DBManager.getConnection().prepareStatement(insertSql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, student.getFirst_name());
            ps.setString(2, student.getLast_name());
            ps.setString(3, student.getPasswort());
            ps.setString(4, student.getBenutzername());
            ps.executeUpdate();
            ResultSet keys = ps.getGeneratedKeys();
            keys.next();
            student.setId((int) keys.getInt(1));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void updateObject(Benutzer student) {
        try {
            String insertSql = "UPDATE " + table_name + " SET first_name = ?, last_name = ?, passwort = ?, benutzername = ? WHERE id = ?";
            PreparedStatement ps = DBManager.getConnection().prepareStatement(insertSql);
            ps.setString(1, student.getFirst_name());
            ps.setString(2, student.getLast_name());
            ps.setString(3, student.getPasswort());
            ps.setString(4, student.getBenutzername());
            ps.setInt(5, student.getId());
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void deleteObject(int id) {
        String sql = "DELETE FROM " + table_name + " WHERE id = ?";
        try {
            PreparedStatement ps = DBManager.getConnection().prepareStatement(sql);
            ps.setInt(1,id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}

