package at.hakimst.db;

import at.hakimst.model.Artikel;
import at.hakimst.model.Bestellung;
import at.hakimst.model.BestellungDetails;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class BestellungDetailsDaoImpl implements IDao<BestellungDetails>{
    private final static String table_name = "bestellung_details";
    private ArtikelDaoImpl artikelDao;
    private BestellungDaoImpl bestellungDao;

    public BestellungDetailsDaoImpl() {
        artikelDao = new ArtikelDaoImpl();
        bestellungDao = new BestellungDaoImpl();
    }

    @Override
    public List<BestellungDetails> getAllObjects (){
        List<BestellungDetails> artikelList = new ArrayList<>();
        String sql = "SELECT * FROM " + table_name;
        try{
            PreparedStatement pst = DBManager.getConnection().prepareStatement(sql);
            ResultSet set = pst.executeQuery();
            int numCols = set.getMetaData().getColumnCount();
            while (set.next()) {
                Artikel a = artikelDao.getObject(set.getInt(2));
                Bestellung b = bestellungDao.getObject(set.getInt(3));
                BestellungDetails bestellungDetails = new BestellungDetails(set.getInt(1), a, b, set.getInt(4));
                artikelList.add(bestellungDetails);
            }
            return artikelList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return artikelList;
    }


    @Override
    public BestellungDetails getObject(int id) {
        String sql = "SELECT * FROM " + table_name + " WHERE id = ?";
        try {
            PreparedStatement ps = DBManager.getConnection().prepareStatement(sql);
            ps.setInt(1,id);
            ResultSet set = ps.executeQuery();
            int numCols = set.getMetaData().getColumnCount();
            set.next();
            Artikel a = artikelDao.getObject(set.getInt(2));
            Bestellung b = bestellungDao.getObject(set.getInt(3));
            BestellungDetails bestellungDetails = new BestellungDetails((int) id, a, b, set.getInt(4));
            return bestellungDetails;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void addObject(BestellungDetails bestellungDetails) {
        try {
            String insertSql = "INSERT INTO " + table_name + "(Artikel_id, Bestellung_id, amount) VALUES (?,?,?)";
            PreparedStatement ps = DBManager.getConnection().prepareStatement(insertSql, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, bestellungDetails.getArtikel().getID());
            ps.setInt(2, bestellungDetails.getBestellung().getId());
            ps.setInt(3, bestellungDetails.getAmount());
            ps.executeUpdate();
            ResultSet keys = ps.getGeneratedKeys();
            keys.next();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateObject(BestellungDetails bestellungDetails) {
        try {
            String insertSql = "UPDATE " + table_name + " SET Artikel_id = ?, Bestellung_id = ?, amount = ? WHERE id = ?";
            PreparedStatement ps = DBManager.getConnection().prepareStatement(insertSql);
            ps.setInt(1, bestellungDetails.getArtikel().getID());
            ps.setInt(2, bestellungDetails.getBestellung().getId());
            ps.setInt(3, bestellungDetails.getAmount());
            ps.setInt(4, bestellungDetails.getId());
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteObject(int id) {
        String sql = "DELETE FROM " + table_name + " WHERE id = ?";
        try {
            PreparedStatement ps = DBManager.getConnection().prepareStatement(sql);
            ps.setInt(1,id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

