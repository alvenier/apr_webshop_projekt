package at.hakimst.db;

import java.util.List;

public interface IDao<E> {

    List<E> getAllObjects();

    E getObject(int i);

    void addObject(E e);

    void updateObject(E e);

    void deleteObject(int i);


}
