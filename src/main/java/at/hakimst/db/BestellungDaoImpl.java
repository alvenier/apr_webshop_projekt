package at.hakimst.db;

import at.hakimst.model.Artikel;
import at.hakimst.model.Benutzer;
import at.hakimst.model.Bestellung;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class BestellungDaoImpl implements IDao<Bestellung> {
    private final static String table_name = "bestellung";
    private BenutzerDaoImpl benutzerDao;
    public BestellungDaoImpl() {
        benutzerDao = new BenutzerDaoImpl();
    }

    @Override
    public List<Bestellung> getAllObjects() {
        List<Bestellung> artikelList = new ArrayList<>();
        String sql = "SELECT * FROM " + table_name;
        try{
            PreparedStatement pst = DBManager.getConnection().prepareStatement(sql);
            ResultSet set = pst.executeQuery();
            int numCols = set.getMetaData().getColumnCount();
            while (set.next()) {
                Benutzer b = benutzerDao.getObject(set.getInt(3));
                Bestellung bestellung = new Bestellung(set.getInt(1), set.getDate(2), b);
                artikelList.add(bestellung);
            }
            return artikelList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return artikelList;
    }

    @Override
    public Bestellung getObject(int id) {
        String sql = "SELECT * FROM " + table_name + " WHERE id = ?";
        try {
            PreparedStatement ps = DBManager.getConnection().prepareStatement(sql);
            ps.setInt(1,id);
            ResultSet set = ps.executeQuery();
            int numCols = set.getMetaData().getColumnCount();
            set.next();
            Benutzer b = benutzerDao.getObject(set.getInt(3));
            Bestellung student =  new Bestellung((int) id, set.getDate(2), b);
            return student;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public void addObject(Bestellung bestellung) {
        try {
            String insertSql = "INSERT INTO " + table_name + "(date, Benutzer_id) VALUES (?,?)";
            PreparedStatement ps = DBManager.getConnection().prepareStatement(insertSql, Statement.RETURN_GENERATED_KEYS);
            ps.setDate(1, new java.sql.Date(bestellung.getDate().getTime()));
            ps.setInt(2, bestellung.getBenutzer().getId());
            ps.executeUpdate();
            ResultSet keys = ps.getGeneratedKeys();
            keys.next();
            bestellung.setId(keys.getInt(1));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateObject(Bestellung bestellung) {
        try {
            String insertSql = "UPDATE " + table_name + " SET date = ?, Benutzer_id = ? WHERE id = ?";
            PreparedStatement ps = DBManager.getConnection().prepareStatement(insertSql);
            ps.setDate(1,new java.sql.Date(bestellung.getDate().getTime()));
            ps.setInt(2, bestellung.getBenutzer().getId());
            ps.setInt(3, bestellung.getId());
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteObject(int id) {
        String sql = "DELETE FROM " + table_name + " WHERE id = ?";
        try {
            PreparedStatement ps = DBManager.getConnection().prepareStatement(sql);
            ps.setInt(1,id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
