package at.hakimst;

import at.hakimst.db.ArtikelDaoImpl;
import at.hakimst.db.BenutzerDaoImpl;
import at.hakimst.db.BestellungDaoImpl;
import at.hakimst.db.BestellungDetailsDaoImpl;
import at.hakimst.model.Artikel;
import at.hakimst.model.Benutzer;
import at.hakimst.model.Bestellung;
import at.hakimst.model.BestellungDetails;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

public class MainController {
    private static MainController instance;
    private Benutzer benutzer = null;
    private BenutzerDaoImpl benutzerDao;
    private ArtikelDaoImpl dao;
    private BestellungDaoImpl bestellungDao;
    private BestellungDetailsDaoImpl detailsDao;
    private List<Artikel> artikelList;
    private List<Artikel> alleartikel;
    private Map<Artikel, Integer> artikelMap;

    @FXML
    private TableView<Artikel> table;

    @FXML
    private TableView<BestellungDetails> warenkorbTable;

    @FXML
    private TableColumn<Artikel, String> warenkorbPName;

    @FXML
    private TableColumn<Artikel, Integer> warenkorbAnzahl;

    @FXML
    private TableColumn<Artikel, Double> warenkorbPreis;

    @FXML
    private TableColumn<Artikel,String> name;

    @FXML
    private TableColumn<Artikel,String> desc;

    @FXML
    private TableColumn<Artikel,Double> price;


    @FXML
    private TextArea textArea, warenkorb;

    @FXML
    private TextField warenkorbadd, anzahl, produktEntfernen, summeWarenkorb;

    @FXML
    private Text euro;

    @FXML
    private Text textfield;

    public MainController(){
        dao = new ArtikelDaoImpl();
        alleartikel = dao.getAllObjects();
        artikelList = new ArrayList<>();
        artikelMap = new HashMap<>();
        instance = this;
    }

    public static MainController getInstance(){
        return instance;
    }

    @FXML
    private void back() throws IOException {
        App.setRoot("login");
    }
    @FXML
    private void test(){

    }
    @FXML
    private void adminlogin() throws IOException {
        App.setRoot("adminlogin");
    }

    /**
     * Eine Bestellung mit den zusätzlichen Bestellung Details wird erzeugt
     */
    @FXML
    public void dieseAndereBestellung(){
        if(!artikelMap.isEmpty()){
            BestellungDaoImpl bestellungDao = new BestellungDaoImpl();
            BestellungDetailsDaoImpl detailDao = new BestellungDetailsDaoImpl();
            Bestellung bestellung = new Bestellung(new Date(), this.benutzer);
            bestellungDao.addObject(bestellung);
            List<BestellungDetails> bestellungDetails = warenkorbTable.getItems();

            bestellungDetails.forEach(detail -> {
                detail.setBestellung(bestellung);
                detailDao.addObject(detail);
            });
            warenkorbTable.getItems().clear();
            artikelMap.clear();
            summeWarenkorb.clear();
        }
    }


    /**
     * Hier wird ein Produkt in den Warenkorb hinzugefügt sofern es auch im Sortiment ist
     */
   @FXML
   public void addW(){
        String name = warenkorbadd.getText().toLowerCase();
        if(name.isEmpty()){
            System.out.println("Bitte geben sie ein Produkt an!");
        }else{
            for(Artikel artikel : alleartikel){
                if(artikel.getName().toLowerCase().contains(name)){
                    int zahl = Integer.parseInt(anzahl.getText());
                    addArtikel(artikel, zahl);
//                artikelList.add(artikel);
                    warenkorbadd.clear();
                    anzahl.clear();
                    refreshWarenkorb();
                }
            }
        }

   }

    /**
     * Produkt wird aus dem Warenkorb entfernt, sofern es im Warenkorb ist
     */
    @FXML
   public void entfernen(){
        String entfernen = produktEntfernen.getText().toLowerCase();
        if(entfernen.isEmpty()){
            System.out.println("Bitte geben sie einen Produktnamen an!");
        }else {
            Artikel artikel = isInMap(produktEntfernen.getText());
            if(artikel != null){
                artikelMap.remove(artikel);
                refreshWarenkorb();
                produktEntfernen.clear();
            }
        }

   }

    /**
     * der Warenkorb wird hier aktualisiert, sowie die Gesamtsumme dessen
     */
   private void refreshWarenkorb(){
        double summe = 0d;
        warenkorbTable.getItems().clear();
        for(Map.Entry<Artikel, Integer> entry : artikelMap.entrySet()){
            summe += entry.getKey().getPrice() * entry.getValue();
            warenkorbTable.getItems().add(new BestellungDetails(entry.getKey(), entry.getValue()));
        }
        summeWarenkorb.clear();
        String textsumme = String.valueOf(summe);
        summeWarenkorb.setText(textsumme + "€");
   }

    /**
     *  Hier wird überprüft oder der Artikel in der Map ist, hierbei wird der name hernangezogen
     *
     * @param name
     * @return Artikel
     */
   private Artikel isInMap(String name){
        for(Map.Entry<Artikel, Integer> entry : artikelMap.entrySet()){
            if(entry.getKey().getName().equalsIgnoreCase(name)){
                return entry.getKey();
            }
        }
        return null;
   }

    /**
     * Hier wird ein Artikel in die Map hinzugefüüht
     * @param artikel
     * @param anzahl
     */
   private void addArtikel(Artikel artikel, int anzahl){
       if(anzahl <= 0 || anzahl > 100){
           System.out.println("Diese Anzahl ist ungültig!");
       } else{
           artikelMap.put(artikel, anzahl);
       }

   }


    /**
     * Hier setzen wir den Benutzer
     * @param b
     */
    public void setBenutzer(Benutzer b){
        this.benutzer = b;
        textfield.setText("Willkommen, " + b.getBenutzername());

    }



//    @FXML
//    public void switchtoW() throws IOException {
//        benutzerDao = new BenutzerDaoImpl();
//        dao = new ArtikelDaoImpl();
//        detailsDao = new BestellungDetailsDaoImpl();
//        bestellungDao = new BestellungDaoImpl();
//        if(artikelList.size() > 0){
//            Bestellung bestellung = new Bestellung(new Date(), benutzer);
//            bestellungDao.addObject(bestellung);
//            for(Artikel artikel : artikelList){
//                detailsDao.addObject(new BestellungDetails(dao.getObject(artikel.getID()), bestellungDao.getObject(bestellung.getId()), 1));
//            }
//            System.out.println("Bestellung wurde erfolgreich aufgenommen");
//        } else{
//            System.out.println("Bitte fügen sie Artikel in den Warenkorb hinzu");
//        }
////        WarenkorbController.getInstance().addall(artikelList);
//    }

    /**
     * Hier werden die Tablen gesetzt
     * Ebenfalls ist hier die Funktion um die Zeilen anzuklicken, dass der Name des Artikels im TextField ausgegeben wird
     */
    @FXML
    private void initialize(){
        dao = new ArtikelDaoImpl();



//        TableColumn id = new TableColumn("id");
//        TableColumn name = new TableColumn("Name");
//        TableColumn beschreibung = new TableColumn("Beschreibung");
//        TableColumn preis = new TableColumn("Preis (€)");

//        table.getColumns().addAll(id, name, desc, price);




        name.setCellValueFactory(new PropertyValueFactory<>("name"));
        desc.setCellValueFactory(new PropertyValueFactory<>("describtion"));
        price.setCellValueFactory(new PropertyValueFactory<>("price"));
        List<Artikel> artikelList = dao.getAllObjects();
        ObservableList<Artikel> observableList = FXCollections.observableList(artikelList);
        table.setItems(observableList);
        table.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                int index = table.getSelectionModel().getFocusedIndex();
                if(index >= 0){
                    Artikel artikel = table.getItems().get(index);
                    warenkorbadd.clear();
                    warenkorbadd.setText(artikel.getName());
                    anzahl.clear();
                    anzahl.setText("1");
                }
            }
        });

        warenkorbTable.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                int index = warenkorbTable.getSelectionModel().getFocusedIndex();
                if(index >= 0){
                    BestellungDetails bestellungDetails = warenkorbTable.getItems().get(index);
                    produktEntfernen.clear();
                    produktEntfernen.setText(bestellungDetails.getArtikelName());
                }
            }
        });


        warenkorbPName.setCellValueFactory(new PropertyValueFactory<>("artikelName"));
        warenkorbAnzahl.setCellValueFactory(new PropertyValueFactory<>("amount"));
        warenkorbPreis.setCellValueFactory(new PropertyValueFactory<>("price"));
        TextField placeHolder = new TextField("Nichts im Warenkorb!");
        placeHolder.setStyle("-fx-background-color: none;");
        placeHolder.setAlignment(Pos.CENTER);
        warenkorbTable.setPlaceholder(placeHolder);
//        String listString = artikelList.stream().map(Objects::toString).collect(Collectors.joining("\n"));
//        textArea.setText(listString);
    }






}
