package at.hakimst;

import java.io.IOException;

import at.hakimst.db.BenutzerDaoImpl;
import at.hakimst.model.Benutzer;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class SignController {

    @FXML
    private TextField field_first, field_last, field_usern;
    @FXML
    private PasswordField passwordfield;

    @FXML
    public void back() throws IOException {
        App.setRoot("login");
    }


    @FXML
    public void sign() throws IOException {
        registrieren();
    }

    /**
     * Hier erstellen wir einen neuen Benutzer sofern er den Anforderungen entspricht
     * @throws IOException
     */
    public void registrieren() throws IOException {
        String vorname = this.field_first.getText().trim();
        String nachname = this.field_last.getText().trim();
        String benutzername = this.field_usern.getText().trim();
        String passwort = this.passwordfield.getText().trim();

        if(vorname.isEmpty() || nachname.isEmpty() || benutzername.isEmpty() || passwort.isEmpty()){
            System.out.println("Bitte füllen sie alle Felder aus!");
        } else{
            if(!invalidUser(vorname, nachname, benutzername)) {
                BenutzerDaoImpl dao = new BenutzerDaoImpl();
                dao.addObject(new Benutzer(vorname, nachname, passwort, benutzername));
                App.setRoot("login");
            }else{
                System.out.println("Ungültiger Name!");
            }
        }
    }

    /**
     * hier überprüfen wir, dass der erstellte Account nicht mit dem Admin Account übereinstimmt
     * @param vorname
     * @param nachname
     * @param username
     * @return
     */
    private boolean invalidUser(String vorname, String nachname, String username){
        Benutzer b = LoginController.admin;
        return b.getFirst_name().equalsIgnoreCase(vorname) || b.getLast_name().equalsIgnoreCase(nachname) || b.getBenutzername().equalsIgnoreCase(username);
    }

}