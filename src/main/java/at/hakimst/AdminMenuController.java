package at.hakimst;

import javafx.fxml.FXML;

import java.io.IOException;

public class AdminMenuController {



    @FXML
    public void manageuser() throws IOException {
        App.setRoot("adduser");
    }

    @FXML
    public void manageproduct() throws IOException {
        App.setRoot("addproducts");
    }

    @FXML
    public void back() throws IOException {
        App.setRoot("login");
    }
}
