package at.hakimst;

import at.hakimst.db.ArtikelDaoImpl;
import at.hakimst.db.BenutzerDaoImpl;
import at.hakimst.model.Artikel;
import at.hakimst.model.Benutzer;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class UserController {
    private BenutzerDaoImpl dao;

    @FXML
    private TableView usertable;

    @FXML
    private TableColumn<Artikel, Integer> idt;

    @FXML
    private TableColumn<Artikel,String> firstt;

    @FXML
    private TableColumn<Artikel,String> lastt;

    @FXML
    private TableColumn<Artikel,String> usert;

    @FXML
    private TableColumn<Artikel, String> passt;

    @FXML
    private TextField lastname, pw, username, id, firstname;

    @FXML
    private TextArea textarea;

    @FXML
    public void back() throws IOException {
        App.setRoot("adminmenu");
    }

    /**
     * Hier wird ein neuer Benuter hinzugefügt
     */
    @FXML
    public void add(){
        String firstn = this.firstname.getText().trim();
        String lastn = this.lastname.getText().trim();
        String password = this.pw.getText().trim();
        String user = this.username.getText().trim();

        if(firstn.isEmpty() || lastn.isEmpty() || password.isEmpty() || user.isEmpty()){
            System.out.println("Bitte füllen sie alle Felder aus");
        } else{

            BenutzerDaoImpl dao = new BenutzerDaoImpl();
            dao.addObject(new Benutzer(firstn, lastn, password, user));

            alleBenutzer();
            lastname.clear();
            pw.clear();
            username.clear();
            firstname.clear();

        }



    }

    @FXML
    private void initialize(){
        alleBenutzer();
    }



    /**
     * Wir geben alle Benutzer in der Tabelle aus
     **/
    public void alleBenutzer(){
        dao = new BenutzerDaoImpl();
        idt.setCellValueFactory(new PropertyValueFactory<>("id"));
        firstt.setCellValueFactory(new PropertyValueFactory<>("first_name"));
        lastt.setCellValueFactory(new PropertyValueFactory<>("last_name"));
        usert.setCellValueFactory(new PropertyValueFactory<>("benutzername"));
        passt.setCellValueFactory(new PropertyValueFactory<>("passwort"));
        List<Benutzer> benutzerList = dao.getAllObjects();
        ObservableList<Benutzer> observableList = FXCollections.observableList(benutzerList);
        usertable.setItems(observableList);
    }
}
