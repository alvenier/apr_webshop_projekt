package at.hakimst;

import at.hakimst.db.ArtikelDaoImpl;
import at.hakimst.model.Artikel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ProductController {
        private ArtikelDaoImpl dao;

        @FXML
        private TextField name, description, price, id;

        @FXML
        private TableView<Artikel> producttable;

        @FXML
        private TableColumn<Artikel, Integer>idt;

        @FXML
        private TableColumn<Artikel,String> namet;

        @FXML
        private TableColumn<Artikel,String> desct;

        @FXML
        private TableColumn<Artikel,Double> pricet;

        @FXML
        private TextArea textarea;

        @FXML
        public void back() throws IOException {
            App.setRoot("adminmenu");
        }

    /**
     * Hier wird ein neuer Artikel mit den eingegebnen Daten erzeugt
     */
    @FXML
    public void add(){
            String pname = this.name.getText().trim();
            String pdesc = this.description.getText().trim();
            String pprice = this.price.getText().trim();

            if(pname.isEmpty() || pdesc.isEmpty() || pprice.isEmpty()){
                System.out.println("Bitte füllen sie alle Felder aus");
            } else{
                Double ppprice = Double.parseDouble(pprice);

                ArtikelDaoImpl dao = new ArtikelDaoImpl();
                dao.addObject(new Artikel(pname, pdesc, ppprice));

                alleProdukte();
                name.clear();
                description.clear();
                price.clear();

            }



        }


        @FXML
        private void initialize(){
            alleProdukte();
        }


    /**
     * Wir geben alle Artikel in der Tabelle aus
     */
    public void alleProdukte(){
            dao = new ArtikelDaoImpl();
            idt.setCellValueFactory(new PropertyValueFactory<>("ID"));
            namet.setCellValueFactory(new PropertyValueFactory<>("name"));
            desct.setCellValueFactory(new PropertyValueFactory<>("describtion"));
            pricet.setCellValueFactory(new PropertyValueFactory<>("price"));
            List<Artikel> artikelList = dao.getAllObjects();
            ObservableList<Artikel> observableList = FXCollections.observableList(artikelList);
            producttable.setItems(observableList);
        }
}
