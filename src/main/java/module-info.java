module at.hakimst {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires mysql.connector.java;

    opens at.hakimst to javafx.fxml;
    opens at.hakimst.model to javafx.base;
    exports at.hakimst;
}